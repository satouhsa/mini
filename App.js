/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */



import Homepage from './src/components/Homepage';
import { Provider } from 'react-redux';
import Login from './src/components/Login';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import React from 'react';


import {Store} from './src/redux/store/Store';
import AddNew from './src/components/AddNew';
import Display from './src/components/Display';
import Profile from './src/components/Profile';
import Menu from './src/components/Menu';

 const Stack = createStackNavigator();
 const Drawer = createDrawerNavigator();
 const DetailStack = createStackNavigator();
 const Tab = createBottomTabNavigator();
 
 const App = () => {
   return (
     <Provider store={Store}>

          <NavigationContainer>
              <Drawer.Navigator>
              
                <Drawer.Screen  name="LoginScreen " component={Login}  />
                <Drawer.Screen
                  name="HomeScreen"
                  component={Homepage}
                  options={{ title: 'HomeScreen',headerStyle:{backgroundColor:'#3caea3'},headerTitleStyle:{fontSize:20,fontWeight:'bold'} }}
                
                />
                <Drawer.Screen  name="Add" component={AddNew}  />
                <Drawer.Screen  name="Dis" component={Display}  />
                <Drawer.Screen  name="Pro" component={Profile}  />
                <Drawer.Screen  name="Man" component={Menu}  />
               
              </Drawer.Navigator>
            </NavigationContainer>

     </Provider>
   
   )
 }
 
 export default App
 