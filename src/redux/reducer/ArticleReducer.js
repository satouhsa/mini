const iniState = {
    allPost: [],
    post: [],
    data:[],
  };
  
  export const postReducer = (state = iniState, action) => {
    switch (action.type) {
      case 'GET_ALL_POST':
        return {
          ...state,
          allPost: action.allPost,
        };
      case 'GET_POST':
        return {
          ...state,
          post: action.post,
        };

        case 'GET_LOGIN':
        return {
          ...state,
          data: action.data,
        };
      default:
        return state;
    }
  };
  