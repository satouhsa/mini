import {combineReducers} from 'redux';
import {postReducer} from '../reducer/ArticleReducer';

const reducers = {
  postReducer: postReducer,
};

export const rootReducer = combineReducers(reducers);
