import axios from 'axios';



export const getAllPost = () => {
  return async (dispatch) => {
    await axios.get(`http://110.74.194.124:15011/v1/api/articles?page=1&limit=15`, {
      headers : {
        "Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="
      }
    }).then((res) => {
      
   
     
      dispatch({
        type: 'GET_ALL_POST',
        allPost: res.data.data,
      });
      console.log( "hell no", res);
    });
  };
};

export const getPost = (id) => {
  return async (dispatch) => {
    await axios.post(`http://110.74.194.124:15011/v1/api/articles`,{
      headers:{
        "Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="
      }
    }).then((res) => {
      dispatch({
        type: 'GET_POST',
        post: res.data.data,
      });
    });
  };
};

export const login =(user)=>{

  return async (dispatch) => {
    await axios.post(`http://110.74.194.124:15011/v1/api/user/authentication`,{
      user:{
        "Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="
      }
    }).then((res) => {
      dispatch({
        type: 'GET_LOGIN',
        data: res.data.data,
      });
    });
  };

}
