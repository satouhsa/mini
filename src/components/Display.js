
import {Body, Card, CardItem, H3, List, ListItem} from 'native-base';
import React, {Component} from 'react';
import {SafeAreaView, Text, View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getPost} from '../redux/action/Article';




export class Display extends Component {

    constructor(props) {
        super(props);
        this.state = {};
      }
    
      componentDidMount() {
        this.props.getPost(this.props.route.params.id);
      }
    render() {
        const {post} = this.props;
        return (
            <SafeAreaView>
                <Card>
                <CardItem>
                   
                     <Text>{post.title}</Text>
                     <Text>{post.description}</Text>
                </CardItem>
                
                </Card>
      </SafeAreaView>
        )
    }
}
const mapStateToProps = (state) => {
    return {
      post: state.postReducer.post,
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
    const boundActionCreators = bindActionCreators(
      {
        getPost,
      },
      dispatch,
    );
    return boundActionCreators;
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(Display);

