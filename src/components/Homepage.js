
import { Container, Header, Content, Card, CardItem, Body,Footer, FooterTab, Button, Icon,List } from 'native-base';
import {  View, FlatList, StyleSheet, Text, StatusBar,Image,TouchableOpacity } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getAllPost,getPost} from '../redux/action/Article'

// import {getAllPost} from '../../redux/actions/post/postAction';

import React, { Component } from 'react'


export class Homepage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      allPost: [],
      post: [],
    };
  }

  componentDidMount() {
    this.props.getAllPost();
    console.log("hhgjggj", this.props.getAllPost());
  }

  disDate(date) {
    let year = date.substring(0, 4);
    let month = date.substring(4, 6);
    let day = date.substring(6, 8);
    return day + "-" + month + "-" + year;
}
  

  // componentDidUpdate(prevProps) {
  //   const {allPost, post} = this.props;
  //   if (prevProps.allPost !== allPost) {
  //     this.setState({
  //       allPost,
  //     });
  //   }

  //   // if (prevProps.post !== post) {
  //   //   this.setState({
  //   //     post,
  //   //   });
  //   // }
  // }

  render() {
    const {navigation} = this.props;
    const {allPost} = this.props;
    return (
      <Container>
      <Header style={{backgroundColor:'#3caea3'}}>

      <View style={{flex: 1, flexDirection: 'row',marginTop:20}}>
      <View style={{flex:1}}>
      <TextInput placeholder="  search" style={{width:165,height:25,backgroundColor:'white',borderRadius:30}}/>
      <Image source={{uri:'https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Search_Icon.svg/1024px-Search_Icon.svg.png'}}
        style={{width:20,height:20,position:'absolute',marginLeft:135,marginTop:4}}
     />

      </View>
      <View >
        <TouchableOpacity onPress={()=>navigation.navigate('TouchBar')}>

        <Image source={{uri:'https://icon-library.com/images/menu-icon/menu-icon-6.jpg'}}
               style={{width:30,height:30,marginRight:50}}
            />

        </TouchableOpacity>
           
      </View>
      <View style={{}}>
          <TouchableOpacity onPress={()=>navigation.navigate('Add')}>
          <Image source={{uri:'https://cdn0.iconfinder.com/data/icons/very-basic-2-android-l-lollipop-icon-pack/24/plus-512.png'}}
                   style={{width:30,height:30}}
              />

          </TouchableOpacity>
              
      </View>
    </View>

          
                
      </Header>
      <Content>

          <Text style={{fontSize:18,fontWeight:'bold',marginTop:20,marginLeft:20,marginBottom:20}}>Your Daily Read</Text>
       

      <List>
        <FlatList
        

          data={allPost}
          keyExtractor={(item) => item.id.toString()}

          renderItem={({item}) => (


            
            <Card>
               
               <TouchableOpacity onPress={()=>navigation.navigate('Dis',{id:item.id})}>
                      <CardItem style={{borderColor:2}} >
                            <View style={{flex: 1, flexDirection: 'row'}}  onPress={()=>navigation.navigate('Display',{id: item.id})}>
                              
                            <View style={{flex:2}}>
                                      <Text style={{fontSize:17,fontWeight:'bold',marginBottom:10}}>{item.title}</Text>
                                      <Text style={{fontSize:16,marginBottom:10}}>{item.description}</Text>
                                        <Text style={{marginBottom:10}}>{item.author.name}</Text>
                                        <Text style={{marginBottom:20}}>{item.created_date}</Text>
                                        <Text >{this.disDate(item.created_date)}</Text>

                            </View>
                              <View>
                                  <Image style={{width:120,height:100}} source={{uri:item.image_url}}></Image>
                              </View>
                            </View>
                      
                      </CardItem>

              
                
                  </TouchableOpacity>
            </Card>
           
         
         
      
           
         
          )}
        />
      </List>
       
      </Content>
      <Footer style={{backgroundColor:'#3caea3'}}>
            <FooterTab>
                  <Button vertical onPress={()=>navigation.navigate('HomeScreen')}>
                    
                  <Image source={{uri:'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Home_Icon.svg/1200px-Home_Icon.svg.png'}}
                      style={{width:30,height:30}}
                  />
                  <Text>Home</Text>
                </Button>
                <Button vertical onPress={()=> navigation.navigate('Pro')}>
                <Image source={{uri:'https://cdn0.iconfinder.com/data/icons/very-basic-2-android-l-lollipop-icon-pack/24/plus-512.png'}}
                   style={{width:35,height:35}}
              />
                  <Text>Profile</Text>
                </Button>
            </FooterTab>
       </Footer>
    </Container>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
 
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderColor:'blue'
  },
  title: {
    fontSize: 32,
  },
});


const TouchBar =()=>{
  return(
    <View>
          <View>
                  <Card>
                          <CardItem>
                          <View style={{flex: 1, flexDirection: 'row'}}>
                                <View>
                                       
                                   
                                         <Button  title="ALL"
                                      type="outline" style={{ width:50, borderRadius:50}}/>
                                       
                                       
                                </View>
                               <View>
                               <Button  title="NEWS"
                                      type="outline" style={{ width:100, borderRadius:50}}/>
                               </View>
                               <View>
                               <Button  title="HOT NEWS"
                                      type="outline" style={{ width:120, borderRadius:50}}/>
                               </View>
                          </View>
                          
                          </CardItem>
                  </Card>
          </View>
    </View>
  )
}
const mapStateToProps = (state) => {
  return {
    allPost: state.postReducer.allPost,
    post: state.postReducer.post,
  };
};

const mapDispatchToProps = (dispatch) => {
  const boundActionCreators = bindActionCreators(
    {
      getAllPost,
      getPost,
      
    },
    dispatch,
  );
  return boundActionCreators;
};

export default connect(mapStateToProps, mapDispatchToProps)(Homepage) 


 


