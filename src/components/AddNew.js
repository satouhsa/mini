import React, { Component } from 'react'


import { Textarea } from 'native-base'

import { ScrollView,Image,TouchableOpacity } from 'react-native'
import {  KeyboardAvoidingView, TextInput, StyleSheet, Text, Platform, TouchableWithoutFeedback, Keyboard ,View } from 'react-native';
import { Card, ListItem, Button, Icon } from 'react-native-elements'
import {SafeAreaView} from 'react-native-safe-area-context';
import ImagePicker from 'react-native-image-picker';


export class AddNew extends Component {

    constructor(props) {
        super(props);
        this.state = {
          resourcePath: {},
          des: '',
        };
      }
    
      selectFile = () => {
        var options = {
          title: 'Select Image',
          customButtons: [
            {
              name: 'customOptionKey',
              title: 'Choose file from Custom Option',
            },
          ],
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
    
        ImagePicker.showImagePicker(options, res => {
          console.log('Response = ', res);
    
          if (res.didCancel) {
            console.log('User cancelled image picker');
          } else if (res.error) {
            console.log('ImagePicker Error: ', res.error);
          } else if (res.customButton) {
            console.log('User tapped custom button: ', res.customButton);
            alert(res.customButton);
          } else {
            let source = res;
            this.setState({
              resourcePath: source,
            });
          }
        });
      };
    
    render() {
        // const {navigate} = this.navigation;
        const {navigation} =this.props
    let {btnPost} = styles;

        return (
            <SafeAreaView>
      

                <View>

                    <View>

                        <Card>

                               <Image
                                source={{uri: this.state.resourcePath.uri}}
                                style={{
                                  backgroundColor: '#eee',
                                  marginTop: 0,
                                  height: 250,
                                  width: 350,
                                  bottom: 0,
                                }}
                              />
                          
                          

                            

                        </Card>

                        <View style={{marginTop:25,marginLeft:20}}>
                                  <TouchableOpacity onPress={this.selectFile}>
                                            <Text style={{fontSize:18,fontWeight:'bold',color:'blue'}} >Select File</Text>
                                  </TouchableOpacity>
                        </View>

                        

                        <View style={{marginLeft:30,marginTop:20}}>

                                <TextInput placeholder="    Title of Article" 
                                style={{backgroundColor:"white",borderColor:2,width:350,height:40,borderRadius:20}}/>


                        </View>

                        <View style={{marginLeft:30,marginTop:20}}>

                            <TextInput placeholder="    Selected Category" 
                            style={{backgroundColor:"white",borderColor:2,width:350,height:40,borderRadius:20}}/>


                            </View>

                            <View style={{marginLeft:30,marginTop:20}}>

                            <TextInput placeholder="    Description" 
                            style={{backgroundColor:"white",borderColor:2,width:350,height:40,borderRadius:20}}/>


                            </View>


                            <View style={{marginLeft:30,marginTop:20}}>

                                    <Button title="post" style={{width:350,height:50}}></Button>
                                 

                              </View>



                    </View>

                </View>

          
      </SafeAreaView>


        )
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
     
    },
    inner: {
      padding: 24,
      flex: 1,
      justifyContent: "space-around"
    },
    header: {
      fontSize: 36,
      marginBottom: 48
    },
    textInput: {
      height: 40,
      borderColor: "#000000",
      borderBottomWidth: 1,
      marginBottom: 36
    },
    btnContainer: {
      backgroundColor: "white",
      marginTop: 12
    }
  });
  
  
export default AddNew

