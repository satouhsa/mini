import React, { Component } from 'react'
import { Text, TextInput, View,Button,StyleSheet,Alert } from 'react-native'
import { Container, Header, Content, Card, CardItem, Body } from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {login} from '../redux/action/Article';




export class Login extends Component {

    constructor(props){
        super(props);
        this.state={
            username:'',
            password:'',

        };
    }

    componentDidMount() {
      this.setState({
        password: "",
        username: "",
      });
    }
  
    display(e) {
      let name = e.target.name;
      let value = e.target.value;
      this.setState({
        [name]: value,
      });
    }
    validationForm = () => {
      let username = this.state.username;
      let password = this.state.password;
      let isValid = true;
      let error = {};
      if (!username) {
        isValid = false;
        error["username"] = "*សូមបញ្ចូលឈ្មោះ";
      }
      if (!password) {
        isValid = false;
        error["password"] = "*សូមបំពេញនូវលេខសំងាត់";
      }
      this.setState({
        error: error,
      });
      return isValid;
    };


    handlesubmit(e, value) {
      e.preventDefault();
      if (this.validationForm()) {
        let user = {
          username: this.state.username,
          password: this.state.password,
        };
        this.props.userLogin(user, (message) => {
          this.setState({
            password: "",
            username: "",
          });
          
        });
      }
    }
  
  
    onLogin() {
        const { username, password } = this.state;
        let user = {
          password: this.state.password,
          username: this.state.username,
        };
        this.props.login(user => {
          this.setState({
            password: "",
            username: "",
          });
       } );
  
    
        // Alert.alert('Credentials', `${username} + ${password}`);
        Alert.alert('Login Successfully');
      }
    render() {
        return (

            <Container>
            {/* <Header style={{backgroundColor:'#3caea3'}}><Text style={{fontSize:20,fontWeight:'bold',marginTop:15,color:"white"}}>Login Screen</Text></Header> */}
            <Content>


             <View style={styles.container}>
                    <Text style={{fontSize:30,fontWeight:'bold',color:'blue',marginBottom:50}}>Hey App</Text>

                     <TextInput
                    value={this.state.username}
                    onChangeText={(username) => this.setState({ username })}
                    placeholder={'Username...'}
                    style={styles.input}
                    />
                    <TextInput
                    value={this.state.password}
                    onChangeText={(password) => this.setState({ password })}
                    placeholder={'Password...'}
                    secureTextEntry={true}
                    style={styles.input}
                    />
               
                    <Button
                    title={'Login'}
                    style={styles.input}
                   
                        
                    // onPress={this.onLogin.bind(this)}
                    onPress ={()=> this.props.navigation.navigate('HomeScreen')}
                    />
                    {/* <Button title={'Login'}
                    style={styles.button}
                    onPress={this.onLogin.bind(this)}></Button> */}
               
            </View>
    
                
            </Content>
          </Container>
           
        )
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
     
      marginTop:200
    },
    input: {
      width: 330,
      height: 44,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 10,
      borderRadius:30,
      backgroundColor:"white",
      
    },
    button:{
        width:330,
        height:44,
        padding:10,
        borderWidth:1,
        backgroundColor:"blue",
        


        
    }
  });
  


  const mapStateToProps = (state) => {
    return {
      
      data:state.postReducer.data
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
    const boundActionCreators = bindActionCreators(
      {
        login
        
      },
      dispatch,
    );
    return boundActionCreators;
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(Login) 



